package com.crecolto.pointmonster.android;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.airbnb.lottie.LottieAnimationView;

public class SplashActivity extends BaseActivity {

    private final String TAG = "MONSTER_SPLASH";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        showProgress();

        LottieAnimationView lottie = (LottieAnimationView) findViewById(R.id.lottie);
        lottie.playAnimation();
        lottie.setRepeatCount(3);

        lottie.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                toastMaker("animation end");
                moveMain();

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    private void moveMain(){

        hideProgress();

        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        finish();

    }

}
