package com.crecolto.pointmonster.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import com.crecolto.pointmonster.android.util.AppUtil;
import com.crecolto.pointmonster.android.util.EAN13CodeBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Hashtable;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private final String TAG = "MONSTER_MAIN";
    private LinearLayout fr_use, fr_save, barcode_view, sample_mission, sample_event, sample_imoji, sample_store, btn_notification;
    private RadioButton btn_save, btn_use;
    private TextView gift_point, charge_point, sale_point;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fr_use = (LinearLayout) findViewById(R.id.fr_use);
        fr_save = (LinearLayout) findViewById(R.id.fr_save);
        barcode_view = (LinearLayout) findViewById(R.id.barcode_view);
        btn_notification = (LinearLayout) findViewById(R.id.btn_notification);

        gift_point = (TextView) findViewById(R.id.gift_point);
        sale_point = (TextView) findViewById(R.id.sale_point);
        charge_point = (TextView) findViewById(R.id.charge_point);

        btn_save = (RadioButton) findViewById(R.id.btn_save);
        btn_use = (RadioButton) findViewById(R.id.btn_use);

/**
 * temp item
 */

        sample_mission = (LinearLayout) findViewById(R.id.sample_mission);
        sample_event = (LinearLayout) findViewById(R.id.sample_event);
        sample_imoji = (LinearLayout) findViewById(R.id.sample_imoji);
        sample_store = (LinearLayout) findViewById(R.id.sample_store);

        sample_mission.setOnClickListener(this);
        sample_event.setOnClickListener(this);
        sample_imoji.setOnClickListener(this);
        sample_store.setOnClickListener(this);
        btn_notification.setOnClickListener(this);
        gift_point.setOnClickListener(this);
        sale_point.setOnClickListener(this);
        charge_point.setOnClickListener(this);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        btn_save.setChecked(false);
        btn_use.setChecked(true);

        inIt();

    }

    private void userRegister(){

        Intent i = new Intent(MainActivity.this, UserAgreementActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

    }

    private Bitmap generateBarcode(String str) {

        try {
            String productId = str;
            Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            Writer codeWriter;
            codeWriter = new Code128Writer();
            BitMatrix byteMatrix = codeWriter.encode(productId, BarcodeFormat.EAN_13,700, 350, hintMap);
            int width = byteMatrix.getWidth();
            int height = byteMatrix.getHeight();
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    bitmap.setPixel(i, j, byteMatrix.get(i, j) ? Color.BLACK : Color.WHITE);
                }
            }
//            imageViewResult.setImageBitmap(bitmap);
            return bitmap;
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return null;
    }

    private void inIt(){

        if(isLogon()){

            Log.d(TAG, "LOGON OK : " + String.valueOf(AppUtil.getPref(this).getBoolean(getResources().getString(R.string.is_logon), false)));

            checkFirstRunning();

        }else{

            Log.d(TAG, "LOGON NOT OK : " + String.valueOf(AppUtil.getPref(this).getBoolean(getResources().getString(R.string.is_logon), false)));

            userRegister();

        }

    }

    private final long FINISH_INTERVAL_TIME = 2000;
    private long backPressedTime = 0;

    @Override
    public void onBackPressed() {

        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime){

            super.onBackPressed();

        }else{

            backPressedTime = tempTime;
            toastMaker(getResources().getString(R.string.item_text_exit));

        }

    }

    @Override
    protected void onResume() {

        super.onResume();

        if(AppUtil.hasUpdateThing()){

            checkFirstRunning();

        }

    }

    private boolean isLogon(){

        /**
         * is logon?
         */

        Log.d(TAG, "LOGON STATUS : " + String.valueOf(AppUtil.getPref(this).getBoolean(getResources().getString(R.string.is_logon), false)));

        boolean tf = AppUtil.getPref(this).getBoolean(getResources().getString(R.string.is_logon), false);

        if(tf){

            return true;

        }else{

            return false;

        }

    }

    private void checkFirstRunning(){

        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        Intent i;

        if(pref.getBoolean(getResources().getString(R.string.need_how_to), true)){

//            SharedPreferences.Editor editor = pref.edit();
//            editor.putBoolean(getResources().getString(R.string.need_how_to), false);
//            editor.commit();

            i = new Intent(MainActivity.this, HowToUseActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        }

            /*Bitmap bitmap = generateBarcode("point_monster");
            ImageView iv = new ImageView(this);
            iv.setImageBitmap(bitmap);*/

        TextView t = (TextView) findViewById(R.id.barcode);
        Typeface font = Typeface.createFromAsset(this.getAssets(),
                "fonts/EanP72Tt_Normal.ttf");
        t.setTypeface(font);
        t.setTextSize(150);
        EAN13CodeBuilder bb = new EAN13CodeBuilder("1234567891011");
        String temp = bb.getCode().substring(1);
        t.setText(temp);

//        barcode_view.removeAllViews();
//        barcode_view.addView(t);

    }

    /**
     * 0 : save
     * 1 : use
     * @param viewType
     */

    private void setViewSelector(int viewType){

        switch (viewType){

            case 0 :

                if(fr_save.getVisibility() != View.VISIBLE){

                    fr_save.setVisibility(View.VISIBLE);
                    fr_use.setVisibility(View.GONE);

                }break;

            case 1 :

                if(fr_use.getVisibility() != View.VISIBLE){

                fr_use.setVisibility(View.VISIBLE);
                fr_save.setVisibility(View.GONE);

            }break;

        }

    }

    private void goToSubpage(String pageTitle){

        Intent i = new Intent(MainActivity.this, SubActivity.class);
        i.putExtra("page_name", pageTitle);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

    }

    /**
     * 0 : gift
     * 1 : charge
     * 2 : sale
     * 3 : notification
     * @param type
     */

    private void setPage(int type){

        Intent i = null;

        switch (type){

            case 0 : i = new Intent(MainActivity.this, PointGiftActivity.class);break;

            case 1 : i = new Intent(MainActivity.this, PointChargeActivity.class);break;

            case 2 : i = new Intent(MainActivity.this, PointSaleActivity.class);break;

            case 3 : i = new Intent(MainActivity.this, NotificationActivity.class);break;

        }

        if(i != null){

            startActivity(i);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        }else{

            toastMaker("intent is null");

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_save :

                btn_save.setChecked(true);
                btn_use.setChecked(false);
                setViewSelector(0);break;

            case R.id.btn_use :

                btn_save.setChecked(false);
                btn_use.setChecked(true);
                setViewSelector(1);break;

/**
 * temp logic
 */

            case R.id.sample_imoji : goToSubpage("몬스터 이모티콘");break;

            case R.id.sample_store : goToSubpage("몬스터 스토어");break;

            case R.id.sample_mission : goToSubpage("몬스터 미션");break;

            case R.id.sample_event : goToSubpage("몬스터 이벤트");break;

            case R.id.btn_notification : setPage(3);break;

            case R.id.sale_point : setPage(2);break;

            case R.id.charge_point : setPage(1);break;

            case R.id.gift_point : setPage(0);break;

        }

    }
}
