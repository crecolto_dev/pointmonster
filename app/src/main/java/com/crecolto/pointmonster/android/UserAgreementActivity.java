package com.crecolto.pointmonster.android;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.crecolto.pointmonster.android.util.AppUtil;

import static com.crecolto.pointmonster.android.util.AppUtil.pref;

public class UserAgreementActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_agreement);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppUtil.setUpdateThing();
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(getResources().getString(R.string.is_logon), true);
        editor.commit();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        finish();
    }
}
