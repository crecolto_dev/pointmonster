package com.crecolto.pointmonster.android;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.crecolto.pointmonster.android.adapter.AdapterHowToUse;
import com.crecolto.pointmonster.android.util.AppUtil;

import me.relex.circleindicator.CircleIndicator;

public class HowToUseActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to);

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        AdapterHowToUse adapterHowToUse = new AdapterHowToUse(this);
        viewPager.setAdapter(adapterHowToUse);

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);

        TextView tv = (TextView) findViewById(R.id.logon_status);
        TextView tv_01 = (TextView) findViewById(R.id.how_to_use_status);
        tv.setText("current logon status : " + String.valueOf(AppUtil.getPref(this).getBoolean(getResources().getString(R.string.is_logon), false)));
        tv_01.setText("current how to use status : " + String.valueOf(AppUtil.getPref(this).getBoolean(getResources().getString(R.string.need_how_to), false)));

        AppUtil.resetUpdateThing();

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_skip : onBackPressed();break;

        }

    }
}
