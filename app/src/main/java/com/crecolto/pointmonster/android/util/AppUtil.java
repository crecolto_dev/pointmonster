package com.crecolto.pointmonster.android.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class AppUtil {

    public static SharedPreferences pref = null;
    public static boolean callByUserAgreement = false;

    public static SharedPreferences getPref(Context context){

        if(pref == null){

            pref = context.getSharedPreferences("pref", MODE_PRIVATE);

        }

        return pref;

    }

    public static void setUpdateThing(){

        callByUserAgreement = true;

    }

    public static void resetUpdateThing(){

        callByUserAgreement = false;

    }

    public static boolean hasUpdateThing(){

        return callByUserAgreement;

    }

}
