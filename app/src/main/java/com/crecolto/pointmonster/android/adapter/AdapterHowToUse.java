package com.crecolto.pointmonster.android.adapter;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.crecolto.pointmonster.android.HowToUseActivity;
import com.crecolto.pointmonster.android.R;

public class AdapterHowToUse extends PagerAdapter {

    private final String TAG = "MONSTER_SPLASH";
    private HowToUseActivity activity = null;

    public AdapterHowToUse(HowToUseActivity activity){
        this.activity = activity;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        TextView iv = new TextView(container.getContext());
        iv.setTextColor(container.getContext().getResources().getColor(R.color.colorWhite));

//        switch (position) {

        iv.setGravity(Gravity.CENTER);
        iv.setTextSize(60);
        iv.setText("current frag " + String.valueOf(position));

            /*case 0 : iv.setText(String.valueOf(position));
                break;

            case 1 : iv.setText(String.valueOf(position));
                break;

            case 2 : iv.setText(String.valueOf(position));
                break;

            case 3:
                break;

            case 4:
                break;

            case 5:
                break;*/

//        }

        container.addView(iv, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return iv;

    }


    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
