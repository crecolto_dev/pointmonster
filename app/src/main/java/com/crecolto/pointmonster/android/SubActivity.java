package com.crecolto.pointmonster.android;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class SubActivity extends BaseActivity {

    TextView title_sub_page, content_sub_page;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        title_sub_page = (TextView) findViewById(R.id.title_sub_page);
        content_sub_page = (TextView) findViewById(R.id.content_sub_page);

        if(getIntent().hasExtra("page_name")){

            title_sub_page.setText(getIntent().getStringExtra("page_name"));
            content_sub_page.setText(getIntent().getStringExtra("page_name") + "\nCONTENT");

        }

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}
